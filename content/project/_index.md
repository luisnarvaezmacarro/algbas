+++
title = "Proyecto docente"
weight = 50
pre = "<i class='fa fa-book'></i> "
+++

{{< button href="../docs/project.pdf" align="center" >}} Documento oficial {{< /button >}}

### ¿Qué vamos a estudiar?

La asigantura se centrará en el estudio de los conceptos de anillo, módulo y cuerpo y sus aplicaciones a las ecuaciones diofánticas, los operadores lineales, las construcciones geométricas con regla y compás y las ecuaciones polinómicas de grado superior en una variable. Seguiremos los capítulos correspondientes del libro titulado [*Algebra*](http://encore.fama.us.es/iii/encore/record/C__Rb1729980), de Michael Artin, basado en las notas que este profesor del Massachussets Institute of Technology usaba en sus clases. 

![book](../images/book.jpeg)

Concretamente usaremos los capítulos del 10 al 14 de la primera edición del libro, que se corresponden esencialmente con los capítulos del 11, 12, 14, 15 y 16 de la segunda edición. 

### ¿Cómo vamos a evaluar?

La evaluación continua se basará en dos exámenes. El primero se realizará entre el 22 y el 24 de noviembre, según el grupo, y el segundo el último día de clase. Los contenidos de cada examen se fijarán con la suficiente antelación. La nota final de la evaluación continua será la media aritmética de ambos exámenes. Los estudiantes que no aprueben por este método podrán presentarse a las convocatorias oficiales en las fechas designadas por la Facultad de Matemáticas bajo las condiciones establecidas por la Universidad de Sevilla. 

Quien apruebe solo uno de los dos exámenes de la evaluación continua y tenga una media suspensa podrá, si así lo desea, presentarse al examen de la primera convocatoria y examinarse solo de la parte que suspendió. Su nota final será la media aritmética de la parte que aprobó y de la que se examine en la primera convocatoria. También podrán presentarse a la primera convocatoria aquellos estudiantes aprobados que deseen subir su nota, en ningún caso la bajarán.


### Clases

Cuatro horas semanales, de las cuales una hora será de problemas, de media a lo largo del semestre.

### Tutorías

Martes y viernes de 8:30 a 11:30.
